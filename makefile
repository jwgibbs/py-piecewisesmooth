# Directory names:
BUILD = build
SRC = pyx_src
INSTALL = /usr/local/JWG/bin/

# Python, Cython and C compiler executables:
PY = /opt/local/bin/py27-MacPorts
CY = /opt/local/bin/cython-2.7
CC = /usr/local/bin/gcc

# Compiling:
CC_OPT=-O3 -fPIC -fopenmp -march=native
INC_DIRS=-I/opt/local/Library/Frameworks/python.framework/Versions/2.7/include/python2.7

# Linking:
LINK_DIRS=-L/opt/local/Library/Frameworks/python.framework/Versions/2.7/lib
LINK_LIBS=-lpython2.7
LINK_OPTS=-shared -fopenmp

# Shared object files:
SO_FILES = pws2D.so pws3D.so pws4D.so 


############## Default make case ###############
all: $(SO_FILES)


######## Auto compilation via distutils ########
distutils:
	$(PY) setup/setup.py build_ext --inplace


#################### 2D class ####################
### Cython:
$(BUILD)/pws2D.c: $(SRC)/pws2D.pyx | $(BUILD)
	$(CY) -a $(SRC)/pws2D.pyx -o $(BUILD)/pws2D.c
### Compile:
$(BUILD)/pws2D.o: $(BUILD)/pws2D.c
	$(CC) $(INC_DIRS) $(CC_OPT) -c $(BUILD)/pws2D.c -o $(BUILD)/pws2D.o
### Link:
pws2D.so: $(BUILD)/pws2D.o
	$(CC) $(LINK_DIRS) $(LINK_LIBS) $(LINK_OPTS) build/pws2D.o -o pws2D.so


#################### 3D class ####################
### Cython:
$(BUILD)/pws3D.c: $(SRC)/pws3D.pyx | $(BUILD)
	$(CY) -a $(SRC)/pws3D.pyx -o $(BUILD)/pws3D.c
### Compile:
$(BUILD)/pws3D.o: $(BUILD)/pws3D.c
	$(CC) $(INC_DIRS) $(CC_OPT) -c $(BUILD)/pws3D.c -o $(BUILD)/pws3D.o
### Link:
pws3D.so: $(BUILD)/pws3D.o
	$(CC) $(LINK_DIRS) $(LINK_LIBS) $(LINK_OPTS) build/pws3D.o -o pws3D.so


#################### 4D class ####################
### Cython:
$(BUILD)/pws4D.c: $(SRC)/pws4D.pyx | $(BUILD)
	$(CY) -a $(SRC)/pws4D.pyx -o $(BUILD)/pws4D.c
### Compile:
$(BUILD)/pws4D.o: $(BUILD)/pws4D.c
	$(CC) $(INC_DIRS) $(CC_OPT) -c $(BUILD)/pws4D.c -o $(BUILD)/pws4D.o
### Link:
pws4D.so: $(BUILD)/pws4D.o
	$(CC) $(LINK_DIRS) $(LINK_LIBS) $(LINK_OPTS) build/pws4D.o -o pws4D.so


# Build dir:
$(BUILD):
	mkdir -p $(BUILD)


install: $(SO_FILES)
	mv pws2D.so $(INSTALL)
	mv pws3D.so $(INSTALL)
	mv pws4D.so $(INSTALL)


clean:
	rm -rf *.so
	rm -rf build
