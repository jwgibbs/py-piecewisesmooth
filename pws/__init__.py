def pws(rho, thr):
    if rho.ndim == 2:
        import pws2D
        return pws2D.PiecewiseSmooth(rho, thr)
    elif rho.ndim == 3:
        import pws3D
        return pws3D.PiecewiseSmooth(rho, thr)
