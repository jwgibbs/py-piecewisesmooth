#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: embedsignature=True

# cython: profile=False

cimport cython
from cython.parallel cimport prange
# ==============================================================
class PiecewiseSmooth():
    
    def __init__(self, rho, thr):
        import numpy as np
        #
        self.rho = rho.copy()
        self.thr = thr
        self.nX = rho.shape[0]
        self.nY = rho.shape[1]
        self.nZ = rho.shape[2]
        #
        self.print_status('Creating arrays')
        self.pos = np.zeros_like(self.rho)
        self.neg = np.zeros_like(self.rho)
        self.phi = np.zeros_like(self.rho)
        self.rec = np.zeros_like(self.rho)
        # ======================================================
        # Initialize the signed distance function:
        self.print_status('Initializing the SDF')
        self.phi = self.rho-self.thr
        self.reinit_phi(verbose=0, tol=0.1, max_it=100, use_weno=False)
        # ======================================================
        # Initialize pos and neg:
        self.print_status("Initializing 'pos' and 'neg'")
        cdef ssize_t  x, nX=self.nX
        cdef ssize_t  y, nY=self.nY
        cdef ssize_t  z, nZ=self.nZ
        cdef int      pos_num=0, neg_num=0
        cdef double   pos_sum=0.0, neg_sum=0.0
        cdef double   cy_thr=self.thr
        cdef double[:,:,:] cy_rho=self.rho
        cdef double[:,:,:] cy_pos=self.pos
        cdef double[:,:,:] cy_neg=self.neg
        # Calculate average values:
        with nogil:
          for x in prange(nX):
            for y in range(nY):
              for z in range(nZ):
                if cy_rho[x,y,z] > cy_thr:
                  pos_sum += cy_rho[x,y,z]
                  pos_num += 1
                else:
                  neg_sum += cy_rho[x,y,z]
                  neg_num += 1
        pos_avg = pos_sum/<double> pos_num
        neg_avg = neg_sum/<double> neg_num
        # Set initial values of pos and neg:
        with nogil:
          for x in prange(nX):
            for y in range(nY):
              for z in range(nZ):
                if cy_rho[x,y,z] > cy_thr:
                  cy_pos[x,y,z] = cy_rho[x,y,z]
                  cy_neg[x,y,z] = neg_avg
                else:
                  cy_pos[x,y,z] = pos_avg
                  cy_neg[x,y,z] = cy_rho[x,y,z]
        # ======================================================
        # Smooth the absorption densities
        self.print_status("Smoothing 'pos' and 'neg'")
        self.smooth_rho(it=50, dt=0.05)
        self.smooth_rho(it=50, dt=0.20)
        # ======================================================
        # Calculate the piecewise smooth reconstruction:
        self.print_status("Creating 'rec'")
        self.update_rec(width=0.5)
        self.print_status("Finished initialization")
        self.print_status("")
    
    def print_status(self, msg):
        from sys import stdout
        #
        stdout.write("   {: <50s}".format(msg))
        stdout.write("\r")
        stdout.flush()
    
    def update_rho(self, err, dt=0.1):
        """
        
        """
        # Initialize pos and neg:
        cdef ssize_t  x, nX=self.nX
        cdef ssize_t  y, nY=self.nY
        cdef ssize_t  z, nZ=self.nZ
        cdef double   cy_dt=dt
        cdef double[:,:,:] cy_err=err
        cdef double[:,:,:] cy_phi=self.phi
        cdef double[:,:,:] cy_pos=self.pos
        cdef double[:,:,:] cy_neg=self.neg
        #
        with nogil:
          for x in prange(nX):
            for y in range(nY):
              for z in range(nZ):
                if cy_phi[x,y,z] > 0.0:
                  cy_pos[x,y,z] += cy_dt*cy_err[x,y,z]
                else:
                  cy_neg[x,y,z] += cy_dt*cy_err[x,y,z]
    
    def smooth_rho(self, it=50, dt=0.1):
        """
        Smooth the two absorption densities (pos and neg)
        """
        from im3D.smoothing import ds
        #
        self.pos = ds(self.pos, it=it, dt=dt)
        self.neg = ds(self.neg, it=it, dt=dt)
    
    def reinit_phi(self, verbose=1, max_it=50, tol=0.5, dt=0.25, use_weno=True):
        from im3D.sdf import reinit
        #
        self.phi = reinit(self.phi, verbose=verbose, tol=tol,
                          dt=dt, max_it=max_it, use_weno=use_weno)
    
    def update_phi(self, err, dt=1.0, verbose=0, max_it=50):
        self.phi += dt*err
        self.reinit_phi(verbose=verbose, max_it=max_it)
    
    def update_rec(self, width=0.5):
        """
        Recalculate rec after changing pos, neg or phi
        """
        from numpy import tanh
        #
        self.rec = self.pos * 0.5*(1.+tanh(self.phi/width)) + \
                   self.neg * 0.5*(1.-tanh(self.phi/width))
    
    def H(self):
        """
        Mean curvature
        """
        from im3D.curvature import H
        #
        return H(self.phi)
    
    def contours(self, z, im=None, vmin=-0.1, vmax=+0.1):
        """
        Draw current (rec) and original (rho) contours on 
        top of 'im'
        """
        import matplotlib.pyplot as plot
        #
        if im == None:
            from im3D.curvature import H
            im = H(self.phi)
        plot.clf()
        plot.imshow(im[...,z], vmin=vmin, vmax=vmax)
        plot.contour(self.rho[...,z], levels=[self.thr], colors='k')
        plot.contour(self.rec[...,z], levels=[self.thr], colors='r')
        plot.draw()

