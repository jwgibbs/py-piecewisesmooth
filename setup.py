import sys, os, stat, commands
from distutils.core import setup
from distutils.extension import Extension
# Check for Cython
try:
    from Cython.Distutils import build_ext
    #from Cython.Build import cythonize
    import Cython.Compiler.Options
    Cython.Compiler.Options.annotate = True
except:
    print('Cython is required for this install')
    sys.exit(1)
# Check for im3D:
try:
    import im3D
except:
    import sys
    print('im3D is required for this install')
    sys.exit(1)

# ==============================================================
# scan a directory for extension files, converting them to
# extension names in dotted notation
def scandir(dir, files=[]):
    for file in os.listdir(dir):
        path = os.path.join(dir, file)
        if os.path.isfile(path) and path.endswith(".pyx"):
            files.append(path.replace(os.path.sep, ".")[:-4])
        elif os.path.isdir(path):
            scandir(path, files)
    return files

# ==============================================================
# generate an Extension object from its dotted name
def makeExtension(extName):
    extPath = extName.replace(".", os.path.sep)+".pyx"
    return Extension(
        extName,
        [extPath],
        include_dirs = ['.'],   # adding the '.' to include_dirs is CRUCIAL!!
        extra_compile_args = ["-O3", "-Wall", "-fPIC"],
        extra_link_args = [],
        libraries = [],
        )

# ==============================================================
# get the list of extensions
extNames = scandir("pws")
# and build up the set of Extension objects
extensions = [makeExtension(name) for name in extNames]
# ==============================================================
setup(
    name="pws",
    version='0.1.0',
    author='John Gibbs',
    author_email='jwgibbs@u.northwestern.edu',
    packages=['pws',],
    description='Piecewise smooth representation of 2-phase data',
    requires=['im3D',],
    ext_modules=extensions,
    cmdclass = {'build_ext': build_ext},
    #ext_modules = cythonize(['pws/pws2D.pyx',
    #                         'pws/pws3D.pyx',])
)
