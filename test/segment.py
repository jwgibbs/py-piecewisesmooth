import numpy as np
import im3D
import pws

import matplotlib.pyplot as plot

im = im3D.io.read_tiff('image.tif').astype(float)
im = im/(2.**8-1)
im = im[312:-312, 312:-312]
seg = pws.pws(im, 0.54)

for i in range(10):
    err = 5. * im3D.smoothing.ds(seg.rho-seg.rec, it=10)
    seg.update_phi(err, verbose=0)
    seg.phi = im3D.smoothing.mmc(seg.phi, dt=0.001, it=50)
    seg.update_rho(err)
    seg.smooth_rho(it=25, dt=0.1)
    seg.update_rec(width=1.0)
    draw_im = plot.imshow(seg.rec, vmin=+0.30,vmax=+0.75)
    plot.draw()

plot.figure(2)
H = im3D.curvature.H(seg.phi)
plot.imshow(H, vmin=-0.1,vmax=0.1)
plot.contour(seg.rho, levels=[seg.thr], colors='b')
plot.contour(seg.rec, levels=[seg.thr], colors='r')
